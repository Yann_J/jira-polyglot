var argv = require('yargs')
    .usage('Usage: $0 [options]')
    .boolean('d')
    .describe('d', 'Debug')

    .number('p')
    .describe('p', 'Port')
    .alias('p', 'port')
    .default('p',3001)

    .string('c')
    .describe('c', 'Context path')
    .alias('c', 'contextpath')
    .default('c','/')

    .string('u')
    .alias('u', 'user')
    .describe('u', 'JIRA username')

    .string('w')
    .alias('w', 'password')
    .describe('w', 'JIRA password')

    .string('h')
    .alias('h', 'host')
    .describe('h', 'JIRA hostname')

    .string('f')
    .alias('f', 'fieldname')
    .describe('f', 'JIRA custom field name to update')

    .help('?')
    .alias('?', 'help')

    .demandOption(['u','w','h','f'])
    .argv;

var config = {
	path     : '/'+argv.c.replace(/^\//,'').replace(/\/$/,''),
	user     : argv.u,
	password : argv.w,
	host     : argv.h,
	fieldname: argv.f,
	debug    : argv.d,
};

var express = require('express');
var bodyParser = require('body-parser');
var compression = require('compression');
var errorHandler = require('errorhandler');
var morgan = require('morgan');
var http = require('http');
var os = require('os');

var routes = require('./routes');
var app = express();

app.use(morgan('combined'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(compression());

routes(app, config);

// Boot the damn thing
try {
	http.createServer(app).listen(argv.p, function(){
		console.log()
		console.log('Add-on server running at '+ ('http://' + (os.hostname()) + ':' + argv.p + config.path));
	});
}
catch (e) {
	console.log(`Could not start server: ${e}`);
}



