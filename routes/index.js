var JiraApi = require('../jira').JiraApi;
var translate = require('google-translate-api');
var util = require('util');
var _ = require('lodash');

module.exports = function (app, config) {
	var jira = new JiraApi(config.host, config.user, config.password, 'latest', config.debug, false);
	var path = config.path || '/';

	app.get(path + 'healthcheck', function (req, res) {
		res.send('OK');
	});

	app.post(path + 'webhook', function (req, res) {
		config.debug && console.log(util.inspect(req.body));

		// Return immediately to not block JIRA
		res.send('OK');

		// Parse...
		var issueKey =         _.get(req.body,'issue.key');
		var issueDescription = _.get(req.body,'issue.fields.description');
		var issueTimestamp =   _.get(req.body,'timestamp');

		if(issueKey && issueDescription) {
			// Translate...
			console.log(`Translating issue ${issueKey}`);
			translate(issueDescription, {from:'auto', to:'en'})
				.then(translated => {
					if(!translated || !translated.text) {
						console.log(`Could not translate issue ${issueKey}: empty response...`);
					}
					else {
						// update
						var update = {};
						_.set(update, 'fields.'+config.fieldname, translated.text);
						console.log(`Posting JIRA update for ${issueKey}`);
						config.debug && console.log(util.inspect(update));
						jira.updateIssue(issueKey, update, (error, status) => {
							console.log(`Results updating issue ${issueKey}: ${error || status}`);
						});
					}
				})
				.catch(error => {
					console.log(`Error translating issue ${issueKey}: ${error}`);
				});
		}
		else {
			console.log(`Could not get proper issue details...`);
		}
	});


};
