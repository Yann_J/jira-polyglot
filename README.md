# Jira Polyglot

Register as a webhook for JIRA issue changes notifications, and it will post English translations of the Description field to a custom field of your choice.

## Installation


```npm install```


## Running

    Usage: app.js [options]
    
    Options:
      -d                  Debug                                            [boolean]
      -p, --port          Port                              [number] [default: 3001]
      -c, --contextpath   Context path                       [string] [default: "/"]
      -u, --user          JIRA username                          [string] [required]
      -w, --password      JIRA password                          [string] [required]
      -h, --host, --help  Show help                              [string] [required]
      -f, --fieldname     JIRA custom field name to update       [string] [required]

    Missing required arguments: u, w, f

## TODO

* Don't translate if source text auto-detected language is already in the target language.
* Check the presence of the custom field in the original issue before trying to update it to avoid JIRA errors.